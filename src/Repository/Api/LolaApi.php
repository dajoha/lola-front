<?php

namespace App\Repository\Api;

use App\Exception\LolaApiException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class LolaApi
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 */
class LolaApi
{
    /** @var HttpClientInterface */
    protected HttpClientInterface $client;

    /**
     * LolaApi constructor.
     *
     * @param HttpClientInterface $lolaApiClient
     */
    public function __construct(HttpClientInterface $lolaApiClient)
    {
        $this->client = $lolaApiClient;
    }

    /**
     * @param string $method
     * @param string $subPath
     * @param array  $options
     *
     * @return array
     *
     * @throws LolaApiException
     */
    public function request(string $method, string $subPath, array $options = []): array
    {
        $path = '/' . ltrim($subPath, '/');

        $headers =  [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
        ];
        if (key_exists('bearer', $options)) {
            $headers['Authorization'] = "bearer {$options['bearer']}";
            unset($options['bearer']);
        }
        if (key_exists('headers', $options)) {
            $headers = array_merge($headers, $options['headers']);
        }
        $options['headers'] = $headers;

        try {
            $response = $this->client->request($method, $path, $options);
            return $response->toArray();
        } catch (
            ClientExceptionInterface | DecodingExceptionInterface |
            RedirectionExceptionInterface | ServerExceptionInterface |
            TransportExceptionInterface $e
        ) {
            throw new LolaApiException($e);
        }
    }

    /**
     * Creates a new lola session.
     *
     * @return array
     *
     * @throws LolaApiException
     */
    public function newSession(): array
    {
        return $this->request('POST', "/sessions");
    }

    /**
     * Clones a lola session.
     *
     * @param string $sessionId
     *
     * @return array
     *
     * @throws LolaApiException
     */
    public function cloneSession(string $sessionId): array
    {
        return $this->request('POST', "/sessions/$sessionId/clone");
    }

    /**
     * Gets information about a session.
     *
     * @param string $sessionId
     *
     * @return array
     *
     * @throws LolaApiException
     */
    public function getSession(string $sessionId): array
    {
        $sessionId = urlencode($sessionId);

        return $this->request('GET', "/sessions/$sessionId");
    }

    /**
     * @return array
     *
     * @throws LolaApiException
     */
    public function getPublicSessions(): array
    {
        return $this->request('GET', "/list_sessions");
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return array
     *
     * @throws LolaApiException
     */
    public function adminLogin(string $username, string $password): array
    {
        return $this->request('POST', "/admin/login", [
            'body' => [
                'username' => $username,
                'password' => $password,
            ],
        ]);
    }

    /**
     * @param string $jwt
     *
     * @return array
     *
     * @throws LolaApiException
     */
    public function adminGetStats(string $jwt): array
    {
        return $this->request('GET', "/admin/stats", [ 'bearer' => $jwt ]);
    }

    /**
     * @param string $jwt
     *
     * @return array
     *
     * @throws LolaApiException
     */
    public function adminPing(string $jwt): array
    {
        return $this->request('GET', "/admin/ping", [ 'bearer' => $jwt ]);
    }
}
