<?php

namespace App\Controller;

use App\Entity\LoginInfo;
use App\Exception\LolaApiException;
use App\Form\LoginType;
use App\Repository\Api\LolaApi;
use App\Security\ConnectionException;
use App\Security\SecurityHandler;
use App\Security\User;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 *
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    protected SecurityHandler $securityHandler;
    protected LoggerInterface $logger;

    /**
     * @param SecurityHandler $securityHandler
     * @param LoggerInterface $logger
     */
    public function __construct(SecurityHandler $securityHandler, LoggerInterface $logger)
    {
        $this->securityHandler = $securityHandler;
        $this->logger = $logger;
    }

    /**
     * @Route("", name="admin")
     *
     * @param LolaApi $lolaApi
     *
     * @return Response
     */
    public function index(LolaApi $lolaApi): Response
    {
        try {
            $jwt = $this->getUser()->getJwt();
            $lolaApi->adminPing($jwt);
        } catch (LolaApiException $e) {
            return $this->handleLolaApiException($e);
        }

        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/login", name="admin_login")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function login(Request $request): Response
    {
        $form = $this->createForm(LoginType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('admin/login/index.html.twig', [
                'form' => $form->createView(),
            ]);
        } else {
            /** @var LoginInfo $loginInfo */
            $login = $form->getData();

            try {
                $this->securityHandler->connect($login->getUsername(), $login->getPassword());
                $this->addFlash('success', "Successfully connected");
                return $this->redirectToRoute('admin');
            } catch (LolaApiException | ConnectionException $e) {
                $this->addFlash('error', $e->getMessage());
                return $this->redirectToRoute('admin_login');
            }
        }
    }

    /**
     * @Route("/logout", name="admin_logout")
     *
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        $this->securityHandler->disconnect();
        $this->addFlash('success', "Disconnected");
        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/stats", name="admin_stats")
     *
     * @param LolaApi $lolaApi
     *
     * @return Response
     */
    public function stats(LolaApi $lolaApi): Response
    {
        try {
            $jwt = $this->getUser()->getJwt();
            $apiResponse = $lolaApi->adminGetStats($jwt);

            if ($apiResponse['success']) {
                return $this->render('admin/stats/index.html.twig', [
                    'stats' => $apiResponse['payload'],
                ]);
            } else {
                $this->addFlash('error', $apiResponse['error']);
                return $this->redirectToRoute('admin');
            }
        } catch (LolaApiException $e) {
            return $this->handleLolaApiException($e);
        }
    }

    /**
     * @return User
     */
    function getUser(): ?User
    {
        return $this->securityHandler->getUser();
    }

    /**
     * @param LolaApiException $e
     *
     * @return RedirectResponse
     */
    function handleLolaApiException(LolaApiException $e): RedirectResponse
    {
        $previous = $e->getPrevious();
        if ($previous->getCode() == 401) {
            $message = "You have been disconnected, please reconnect";
        } else {
            $message = "An error occured from the lola API, disconnecting";
            $this->logger->error($previous->getMessage());
        }
        $this->addFlash('error', $message);
        $this->securityHandler->disconnect();
        return $this->redirectToRoute('admin_login');
    }
}
