<?php

namespace App\Controller;

use App\Exception\LolaApiException;
use App\Repository\Api\LolaApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PublicController
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 */
class PublicController extends AbstractController
{
    /**
     * @Route("/", options={"expose"=true}, name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/new-session", methods={"get"}, options={"expose"=true}, name="new_session")
     *
     * @param LolaApi $lolaApi
     *
     * @return RedirectResponse
     *
     * @throws LolaApiException
     */
    public function newSession(LolaApi $lolaApi): RedirectResponse
    {
        $response = $lolaApi->newSession();

        if ($response['success']) {
            $sessionId = $response['payload']['session_id'];
            return $this->redirectToRoute('session', [ 'sessionId' => $sessionId ]);
        } else {
            // TODO: push flash error message
            // TODO: test this error
            return $this->redirectToRoute('home');
        }
    }

    /**
     * @Route("/sessions/{sessionId}/clone", methods={"get"}, options={"expose"=true}, name="clone_session")
     *
     * @param string  $sessionId
     * @param LolaApi $lolaApi
     *
     * @return RedirectResponse
     *
     * @throws LolaApiException
     */
    public function cloneSession(string $sessionId, LolaApi $lolaApi): RedirectResponse
    {
        $response = $lolaApi->cloneSession($sessionId);

        if ($response['success']) {
            $sessionId = $response['payload']['session_id'];
            return $this->redirectToRoute('session', [ 'sessionId' => $sessionId ]);
        } else {
            // TODO: push flash error message
            // TODO: test this error
            return $this->redirectToRoute('home');
        }
    }

    /**
     * @Route("/sessions/{sessionId}", methods={"get"}, options={"expose"=true}, name="session")
     *
     * @param string  $sessionId
     * @param LolaApi $lolaApi
     *
     * @return Response
     *
     * @throws LolaApiException
     */
    public function session(string $sessionId, LolaApi $lolaApi): Response
    {
        $response = $lolaApi->getSession($sessionId);

        $sessionTitle = null;
        // If the session has been renamed (not only random numbers), set a session title:
        if (preg_match('/^\d{16,}$/', $sessionId) !== 1) {
            $sessionTitle = $sessionId;
        }

        if ($response['success']) {
            $session = $response['payload']['session'];
            return $this->render('session/index.html.twig', [
                'session' => $session,
                'session_title' => $sessionTitle,
            ]);
        } else {
            $this->addFlash('error', "La session demandée n'existe pas");
            return $this->redirectToRoute('home');
        }
    }

    /**
     * @Route("/sessions", methods={"get"}, options={"expose"=true}, name="list_public_sessions")
     *
     * @param LolaApi $lolaApi
     *
     * @return Response
     *
     * @throws LolaApiException
     */
    public function listPublicSessions(LolaApi $lolaApi): Response
    {
        $response = $lolaApi->getPublicSessions();

        return $this->render('list_public_sessions/index.html.twig', [
            'sessions' => $response['payload']['sessions'],
        ]);
    }
}
