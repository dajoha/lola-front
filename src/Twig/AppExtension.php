<?php

namespace App\Twig;

use App\Security\SecurityHandler;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class AppExtension
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 */
class AppExtension extends AbstractExtension
{
    protected SecurityHandler $securityHandler;

    /**
     * @param SecurityHandler $securityHandler
     */
    public function __construct(SecurityHandler $securityHandler)
    {
        $this->securityHandler = $securityHandler;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('checked', [$this, 'checked'], ['is_safe' => ['html']]),
            new TwigFunction('session_path', [$this, 'sessionPath'], ['is_safe' => ['html']]),
            new TwigFunction('is_admin', [$this->securityHandler, 'isAdmin']),
        ];
    }

    /**
     * Checks a checkbox or not.
     *
     * @param bool $value
     *
     * @return string
     */
    public function checked(bool $value): string
    {
        return $value ? 'checked="checked"' : '';
    }

    /**
     * Returns a url to the given session id.
     *
     * @param string $sessionId
     *
     * @return string
     */
    public function sessionPath(string $sessionId): string
    {
        // Just in case, but not really required:
        $sessionId = urlencode($sessionId);

        return "/sessions/$sessionId";
    }
}
