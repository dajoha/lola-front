<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;

/**
 * Class LolaApiException
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 */
class LolaApiException extends Exception
{
    /**
     * @param Exception|null $inner
     */
    public function __construct(?Exception $inner = null)
    {
        parent::__construct("Unable to connect to the lola API", 0, $inner);
    }
}
