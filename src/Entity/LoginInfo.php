<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * Class LoginInfo
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 */
class LoginInfo
{
    protected ?string $username;
    protected ?string $password;

    public function __construct()
    {
        $this->username = null;
        $this->password = null;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     *
     * @return LoginInfo
     */
    public function setUsername(?string $username): LoginInfo
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     *
     * @return LoginInfo
     */
    public function setPassword(?string $password): LoginInfo
    {
        $this->password = $password;
        return $this;
    }
}
