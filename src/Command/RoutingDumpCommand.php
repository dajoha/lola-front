<?php

namespace App\Command;

use App\Service\CommandUtil;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RoutingDumpCommand
 *
 * @category lola-front
 * @package  lola-front
 * @author   Johann David <johann.david.dev@protonmail.com>
 */
class RoutingDumpCommand extends Command
{
    protected static $defaultName = 'app:routing:dump';

    /**
     * @var CommandUtil
     */
    protected $commandUtil;

    /**
     * PostcardsDataResetCommand constructor.
     *
     * @param CommandUtil $commandUtil
     */
    public function __construct(CommandUtil $commandUtil)
    {
        parent::__construct(self::$defaultName);
        $this->commandUtil = $commandUtil;
    }

    protected function configure()
    {
        $this
            ->setDescription('Dumps the json routing file')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->commandUtil->runCommand($output,
            'fos:js-routing:dump',
            [
                '--format'       => 'json',
                '--target'       => 'assets/js/routes.json',
                '--pretty-print' => true,
            ]
        );

        return 0;
    }
}
