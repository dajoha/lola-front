import '../../css/pages/home.scss';

const $ = require('jquery');

const { route } = require('../include/routing');
const lolaApi = require('../lolaApi');
const { SessionNameTextInput, State } = require('../include/session_name_text_input');

let $createSessionBtn;
let sessionInput;

$(() => {
    $createSessionBtn = $('.create-session--btn');
    sessionInput = handleCreateSessionInput();
    $createSessionBtn.click(createSession);
});

/**
 * Handles the session creation text input.
 *
 * @return {SessionNameTextInput}
 */
function handleCreateSessionInput() {
    const $createSessionBtnTitle = $('.create-session--btn__title');
    const $sessionNameInput = $('.session-name--input');
    const $errorMessage = $('.create-session__error-message');

    return new SessionNameTextInput({
        input: $sessionNameInput,
        messageSpan: $errorMessage,
        preValidate: sessionInput => {
            if (sessionInput.name() === '') {
                return State.VALID;
            }
        },
        invalidMessage: session => session.name() === '' ? '' : "Nom déjà existant",
        onStateChange: sessionInput => {
            const [ state, name ] = [ sessionInput.state, sessionInput.name() ];

            $createSessionBtn.prop('disabled', name !== '' && !state.isValid());

            let title;
            if (name === '') {
                const successClass = sessionInput.stateInfo(State.VALID).inputClass;
                $sessionNameInput.removeClass(successClass);
                title = "Créer une session anonyme";
            } else {
                title = "Créer cette session";
            }
            $createSessionBtnTitle.html(title);

            if (sessionInput.enterWasPressed && sessionInput.state.isValid()) {
                sessionInput.confirm();
                createSession();
            }
        },
    });
}

/**
 * Creates a session (named or not, following the value of the session name input), and redirects
 * to it.
 */
function createSession() {
    const sessionName = sessionInput.name();
    const isEmptyName = sessionName === '';

    if (isEmptyName) {
        window.location = route('new_session');
    } else {
        lolaApi.createSession(sessionName)
        .then(() => {
            window.location = route('session', {
                sessionId: sessionName,
            });
        })
    }
}
