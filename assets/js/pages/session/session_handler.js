const $ = require('jquery');
const copyToClipboard = require('copy-to-clipboard');

const { SessionCommand } = require('./session_command');
const modal = require('../../include/modal');

class SessionHandler {
    constructor() {
        // Important sections of the web page:
        this.$sessionLive = $('.session-live');
        this.$sessionHistory = $('.session-history');

        // History handling (Up/Down arrow keys):
        this.historyCommands = [];
        this.currentLiveCommand = null;
        this.lastLiveInput = null;
        this.historyIndex = -1;

        // Show a warning when trying to change the history:
        this.changeHistoryWarning = true;

        // Create the live command block:
        this.newLiveCommand();

        // Load the history:
        this.reloadHistory();

        modal.setupActionAndButton(
            'copy_to_clipboard',
            {
                key: 'c',
                close: false,
            },
            {
                title: `
                    <i class="fas fa-clipboard text--blue"></i>
                    Copier dans le presse-papier
                `,
            }
        );
    }

    /**
     * Creates a new empty command and insert it into the dom.
     * Scrolls down to the bottom of the page in order to make visible to bottom margin.
     *
     * @return {SessionCommand}
     */
    newLiveCommand() {
        const command = new SessionCommand(this);
        this.currentLiveCommand = command;
        this.$sessionLive.append(command.$dom);

        setTimeout(() => {
            const $frame = $("main");
            $frame.animate({ scrollTop: $frame.outerHeight() }, 1);
            command.$input.focus();
        }, 1);

        return command;
    }

    /**
     * Inserts a new empty command before the given history command.
     *
     * @param {SessionCommand} sessionCommand
     *
     * @return {SessionCommand}
     */
    insertLiveCommandBefore(sessionCommand) {
        const command = new SessionCommand(this);
        command.setInsertedBefore(sessionCommand.commandId);

        sessionCommand.$dom.before(command.$dom);
        command.$input.focus();

        this.currentLiveCommand = command;

        return command;
    }

    /**
     * Removes all the history commands and reload them from the server.
     */
    reloadHistory() {
        this.$sessionHistory.empty();

        // Create the command history blocks:
        for (const command of app.session.commands) {
            const sessionCommand = new SessionCommand(this, command);
            this.pushToHistory(sessionCommand);
        }
    }

    /**
     * Move a command dom at the end of the history, and save it into the global
     * array `historyCommands`.
     *
     * @param {SessionCommand} sessionCommand
     */
    pushToHistory(sessionCommand) {
        this.$sessionHistory.append(sessionCommand.$dom);
        this.historyCommands.push(sessionCommand);
    }

    /**
     * Update the content of the code input, after `historyIndex` has been changed.
     */
    updateHistoryCompletion() {
        let input;

        if (this.historyIndex === -1) {
            input = this.lastLiveInput;
        } else {
            let index = this.historyCommands.length - 1 - this.historyIndex;
            if (index < 0 || index >= this.historyCommands.length) {
                return;
            }
            const command = this.historyCommands[index];
            input = command.getInput();
        }

        this.currentLiveCommand.setInput(input);
    }

    /**
     * Go back into history (Up arrow key).
     */
    historyBack() {
        if (this.historyIndex === -1) {
            this.lastLiveInput = this.currentLiveCommand.getInput();
        }
        if (this.historyIndex < this.historyCommands.length - 1) {
            this.historyIndex++;
            this.updateHistoryCompletion();
        }
    }

    /**
     * Go forward into history (Down arrow key).
     */
    historyForward() {
        if (this.historyIndex >= 0) {
            this.historyIndex--;
            this.updateHistoryCompletion();
        }
    }

    /**
     * Opens a modal which shows a warning about changing the history, unless the button "Don't ask
     * me again" has already been clicked. In this case, run the callback without confirmation.
     *
     * @param {string}   modalId The modal to show if needed
     * @param {function} callback The callback function
     */
    confirmChangeHistory(modalId, callback) {
        if (this.changeHistoryWarning) {
            modal.show(modalId, {
                confirm: callback,
                ok_no_more_warning: () => {
                    this.changeHistoryWarning = false;
                    callback();
                }
            });
        } else {
            callback();
        }
    }

    /**
     * Returns the session data for exporting.
     *
     * @return {string}
     */
    getExportData() {
        const exportData = {
            history: app.session.commands.map(command => command.input)
        };

        return JSON.stringify(exportData, null, 2);
    }

    /**
     * Shows the export session modal.
     */
    export() {
        const data = this.getExportData();

        const exportModal = modal.show('modal--export-session', {
            copy_to_clipboard: () => {
                copyToClipboard(data);
                $copyButton.addClass('green-flash');
                setTimeout(() => {
                    $copyButton.removeClass('green-flash');
                }, 500);
            }
        });

        const $dom = $(exportModal.dom);
        const $copyButton = $dom.find('.modal__button--copy_to_clipboard');
        const $dataDiv = $dom.find('.json-export--data');

        $dataDiv
            .text(data)
            .click(ev => ev.stopPropagation())
    }
}

/**
 * Starts the session handler.
 */
function start() {
    return new SessionHandler();
}

module.exports = {
    start,
    SessionHandler,
}
