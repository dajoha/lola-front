const { Modal } = require('../../include/modal');

/**
 * Show an error message in a modal.
 *
 * @param {string} msg
 */
function showError(msg) {
    const modal = new Modal('modal--error');

    const placeholder = modal.dom.querySelector('.modal--error__placeholder');
    const monospace = document.createElement('span');
    monospace.classList.add('text--monospace');
    monospace.innerText = msg;
    placeholder.appendChild(monospace);

    modal.open();
}

module.exports = {
    showError,
}
