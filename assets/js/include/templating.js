/**
 * Return a new dom tree of the given template.
 *
 * @param {string} templateId
 */
function getTemplate(templateId) {
    const template = document.querySelector(`#${templateId}`);
    return document.importNode(template.content, true).children[0];
}

module.exports = {
    getTemplate,
}
