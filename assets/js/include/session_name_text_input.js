const { debounce } = require('../util');
const lolaApi = require("../lolaApi");
const $ = require("jquery");

/**
 * @typedef {Object} StateInfo
 * @property {string} inputClass
 * @property {string|function(SessionNameTextInput)} message
 */

/**
 * @typedef {jQuery|HTMLElement|string} AnyElement
 */

/**
 * @class
 */
class SessionNameTextInput {
    /**
     * @param {Object} options
     * @param {AnyElement} options.input
     * @param {AnyElement} options.messageSpan
     * @param {string?} options.initialName
     * @param {function(SessionNameTextInput)?} options.preValidate
     * @param {string?} options.validInputClass
     * @param {string?} options.invalidInputClass
     * @param {string?} options.pendingInputClass
     * @param {function(SessionNameTextInput):string|string?} options.validMessage
     * @param {function(SessionNameTextInput):string|string?} options.invalidMessage
     * @param {function(SessionNameTextInput):string|string?} options.pendingMessage
     * @param {function(SessionNameTextInput)?} options.onStateChange
     * @param {number?} options.debounceTime
     * @param {bool?} options.updateOnEnter
     */
    constructor(options = {}) {
        if (options.input === undefined) {
            throw "Option `input` is required";
        }
        this.$input = $(options.input);
        this.$messageSpan = $(options.messageSpan);
        this.initialName = options.initialName;
        this.preValidate = options.preValidate;
        this.onStateChange = options.onStateChange;
        this.debounceTime = options.debounceTime ?? 200;
        this.updateOnEnter = options.updateOnEnter ?? true;
        this.enterWasPressed = false;
        this.activated = true;

        /**
         * @type {Object}
         * @property {StateInfo} valid
         * @property {StateInfo} invalid
         * @property {StateInfo} pending
         */
        this.infoByState = {
            valid: {
                inputClass: options.validInputClass ?? 'input--success',
                message: options.validMessage ?? '',
            },
            invalid: {
                inputClass: options.invalidInputClass ?? 'input--error',
                message: options.invalidMessage ?? '',
            },
            pending: {
                inputClass: options.pendingInputClass ?? '',
                message: options.pendingMessage ?? '',
            },
        };

        this.setState(State.PENDING);
        if (typeof this.initialName === 'string') {
            this.$input.val(this.initialName);
        }
        this.focus();

        this.updateInputDebounce = debounce(this.updateInput.bind(this), this.debounceTime);

        this.initKeyboard();

        // Don't close the modal when clicking on the text input:
        this.$input.click(ev => ev.stopPropagation());
    }

    initKeyboard() {
        this.updateInputDebounce();

        this.$input.on('input', () => {
            this.setState(State.PENDING);
            this.updateInputDebounce();
        });
        this.$input.keydown(ev => {
            const key = ev.originalEvent.key;
            if (!['Enter', 'Escape'].includes(key)) {
                ev.stopPropagation();
            }
            if (key === 'Enter') {
                this.enterWasPressed = true;
                if (this.updateOnEnter) {
                    this.updateInput();
                }
            }
        });
    }

    confirm() {
        this.activated = false;
        setTimeout(() => this.activated = true, 1000);
    }

    updateInput() {
        if (!this.activated) {
            return;
        }

        if (typeof this.preValidate === 'function') {
            const state = this.preValidate(this);
            if (state instanceof State) {
                this.setState(state);
                return;
            }
        }
        const sessionId = this.name();
        lolaApi.checkSessionAvailability(sessionId, isValid => {
            this.setState(State.fromBool(isValid));
        });
    }

    /**
     * Get the actual entered session name.
     *
     * @return {string}
     */
    name() {
        return this.$input.val();
    }

    /**
     * Set the actual entered session name.
     *
     * @param {string} name
     */
    setName(name) {
        this.$input.val(name);
    }

    /**
     * Focus on the intput.
     */
    focus() {
        this.$input.focus().select();
    }

    /**
     * @param {State} state
     * @return {StateInfo}
     */
    stateInfo(state) {
        return this.infoByState[state.toString()];
    }

    /**
     * @return {string[]}
     */
    allInputClasses() {
        return State.allStates().map(state => this.infoByState[state].inputClass);
    }

    /**
     * Sets the actual state.
     *
     * @param {State} state
     */
    setState(state) {
        this.state = state;
        this.updateState();
    }

    updateState() {
        const currentStateInfo = this.stateInfo(this.state);
        this.$input.removeClass(this.allInputClasses());
        this.$input.addClass(currentStateInfo.inputClass);

        let message;
        if (typeof currentStateInfo.message === 'function') {
            message = currentStateInfo.message(this);
        } else {
            message = currentStateInfo.message;
        }
        this.$messageSpan.html(message);

        if (typeof this.onStateChange === 'function') {
            this.onStateChange(this);
        }

        this.enterWasPressed = false;
    }
}

class State {
    static VALID = Object.freeze(new State('valid'));
    static INVALID = Object.freeze(new State('invalid'));
    static PENDING = Object.freeze(new State('pending'));

    constructor(state) {
        this.inner_state = state;
    }

    /**
     * @param {boolean} is_valid
     * @return {State}
     */
    static fromBool(is_valid) {
        if (is_valid) {
            return State.VALID;
        } else {
            return State.INVALID;
        }
    }

    /**
     * @return {Readonly<State>[]}
     */
    static allStates() {
        return [ State.VALID, State.INVALID, State.PENDING ];
    }

    /**
     * @return {string}
     */
    toString() {
        return this.inner_state;
    }

    /**
     * @return {boolean}
     */
    isValid() {
        return this === State.VALID;
    }

    /**
     * @return {boolean}
     */
    isInvalid() {
        return this === State.INVALID;
    }

    /**
     * @return {boolean}
     */
    isPending() {
        return this === State.PENDING;
    }
}

module.exports = { SessionNameTextInput, State };
