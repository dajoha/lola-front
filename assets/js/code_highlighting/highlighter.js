const regexp = require('../include/regexp');

/**
 * @typedef TokenDef
 * @property {string} name
 * @property {RegExp} regex
 * @property [LanguageMatcher} innerHighlighter
 */

/**
 * @callback highlightCallback
 * @param {string} match - The matched string
 * @param {TokenDef} def - The token definition
 * @return string
 */

/**
 *
 * @param {string} name
 * @param def
 * @return {TokenDef}
 */
function toTokenDef(name, def) {
    if (typeof def === 'string' || def instanceof RegExp) {
        return {
            name,
            regex: new RegExp(def),
        };
    }
    if (Array.isArray(def) && def[1] instanceof Highlighter) {
        const tokenDef = toTokenDef(name, def[0]);
        tokenDef.innerHighlighter = def[1];
        return tokenDef;
    }
    throw `Unable '${name}' to convert to token def: ${def}`;
}

/**
 * Groups all the regular expressions needed for syntax highlighting.
 */
class Highlighter {
    /**
     * @param {highlightCallback} transformer
     * @param {Object?} defs
     */
    constructor(transformer, defs) {
        /** @type {TokenDef[]} */
        this.tokenDefs = [];
        /** @type {RegExp|null} */
        this.regex = null;
        /** @type {highlightCallback} */
        this.transform = transformer;

        if (typeof defs === 'object') {
            this.add(defs);
        }
    }

    /**
     * Adds regular expressions which match a kind of language token.
     * Important: the given regex MUST NOT include capturing groups.
     *
     * @param {Object} defs
     */
    add(defs) {
        for (name in defs) {
            this.tokenDefs.push(toTokenDef(name, defs[name]));
        }
        this.regex = null;
    }

    /**
     * Returns the whole regex to be used for highlighting.
     *
     * @return {RegExp}
     */
    getRegex() {
        if (this.regex === null) {
            const regexes = this.tokenDefs.map(def => def.regex.source);
            this.regex = regexp.any(regexes, 'g', true);
        }
        return this.regex;
    }

    /**
     * Highlights the given code.
     *
     * @param {string} code
     *
     * @return {string}
     */
    highlight(code) {
        return code.replace(this.getRegex(), (match, ...args) => {
            const i = args.findIndex(arg => arg !== undefined);
            if (i === -1 || i >= this.tokenDefs.length) {
                return match;
            }
            const tokenDef = this.tokenDefs[i];
            if (tokenDef.innerHighlighter instanceof Highlighter) {
                match = tokenDef.innerHighlighter.highlight(match);
            }
            return this.transform(match, tokenDef);
        });
    }
}

module.exports = { Highlighter }
