const { urlEncodedBody } = require('./util');

const API_URL = app.lolaApiUrl;

/**
 * @typedef ApiResponse
 * @template T
 * @property {boolean} success
 * @property {string?} error
 * @property {T} payload
 */

/**
 * @typedef {Object} ApiSession
 * @property {string} session_id
 * @property {boolean} is_public
 * @property {ApiSessionCommand[]} commands
 */

/**
 * @typedef {Object} ApiSessionCommand
 * @property {string} command_id
 * @property {string} input
 * @property {string} output
 * @property {ApiCommandResult} result
 */

/**
 * @typedef {Object} ApiCommandResult
 * @property {string?} ok
 * @property {string?} error
 */

/**
 * @typedef {Object} ApiSessionInformation
 * @property {string} session_id
 * @property {boolean} is_public
 * @property {ApiSessionCommand[]} commands
 */

/**
 * Pings a session.
 *
 * @param {string} sessionId
 *
 * @return {Promise<ApiResponse<undefined>>}
 */
function pingSession(sessionId) {
    return fetch(`${API_URL}/sessions/${sessionId}/ping`, {
        method: 'GET',
    })
    .then(response => response.json())
}

/**
 * Creates a new session.
 *
 * @param {string} sessionId  Named session to create
 *
 * @return {Promise<ApiResponse<{session_id:string}>>}
 */
function createSession(sessionId) {
    const body = urlEncodedBody({ session_id: sessionId });

    return fetch(`${API_URL}/sessions`, {
        method: 'POST',
        body,
    })
    .then(response => response.json())
}

/**
 * Runs some code into the given session.
 *
 * @param {string} sessionId
 * @param {string} newName
 *
 * @return {Promise<ApiResponse<undefined>>}
 */
function renameSession(sessionId, newName) {
    const body = urlEncodedBody({ session_id: newName });

    return fetch(`${API_URL}/sessions/${sessionId}/rename`, {
        method: 'PUT',
        body,
    })
    .then(response => response.json())
}

/**
 * Deletes a session.
 *
 * @param {string} sessionId
 *
 * @return {Promise<ApiResponse<undefined>>}
 */
function deleteSession(sessionId) {
    return fetch(`${API_URL}/sessions/${sessionId}`, {
        method: 'DELETE',
    })
    .then(response => response.json())
}

/**
 * Resets a session.
 *
 * @param {string} sessionId
 *
 * @return {Promise<ApiResponse<undefined>>}
 */
function resetSession(sessionId) {
    return fetch(`${API_URL}/sessions/${sessionId}/reset`, {
        method: 'PUT',
    })
    .then(response => response.json())
}

/**
 * Runs some code into the given session.
 *
 * @param {string} sessionId
 * @param {string} code
 *
 * @return {Promise<ApiResponse<{commands:ApiSessionCommand[]}>>}
 */
function runCode(sessionId, code) {
    const body = {
        code: [ code ]
    };

    return fetch(`${API_URL}/sessions/${sessionId}/run`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    })
    .then(response => response.json())
}

/**
 * Runs multiple commands into the given session.
 *
 * @param {string}   sessionId
 * @param {string[]} code
 *
 * @return {Promise<ApiResponse<{commands:ApiSessionCommand[]}>>}
 */
function runMultipleCode(sessionId, code) {
    const body = {
        code,
    };

    return fetch(`${API_URL}/sessions/${sessionId}/run`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    })
    .then(response => response.json())
}

/**
 * Edits the given command of the given session.
 *
 * @param {string} sessionId
 * @param {string} commandId
 * @param {string} code
 *
 * @return {Promise<ApiResponse<{session_information:ApiSessionInformation}>>}
 */
function editCommand(sessionId, commandId, code) {
    const body = new URLSearchParams();
    body.append('code', code);

    return fetch(`${API_URL}/sessions/${sessionId}/commands/${commandId}`, {
        method: 'PUT',
        body,
    })
    .then(response => response.json())
}

/**
 * Inserts a new command before the given command of the given session.
 *
 * @param {string} sessionId
 * @param {string} beforeCommandId
 * @param {string} code
 *
 * @return {Promise<ApiResponse<{session_information:ApiSessionInformation}>>}
 */
function insertCommand(sessionId, beforeCommandId, code) {
    const body = {
        code: [ code ],
        before_command_id: beforeCommandId,
    };

    return fetch(`${API_URL}/sessions/${sessionId}/commands`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    })
    .then(response => response.json())
}

/**
 * Inserts multiple new commands before the given command of the given session.
 *
 * @param {string}   sessionId
 * @param {string}   beforeCommandId
 * @param {string[]} code
 *
 * @return {Promise<ApiResponse<{session_information:ApiSessionInformation}>>}
 */
function insertMultipleCommands(sessionId, beforeCommandId, code) {
    const body = {
        code,
        before_command_id: beforeCommandId,
    };

    return fetch(`${API_URL}/sessions/${sessionId}/commands`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    })
    .then(response => response.json())
}

/**
 * Deletes a command in the given session.
 *
 * @param {string} sessionId
 * @param {string} commandId
 *
 * @return {Promise<ApiResponse<{session_information:ApiSessionInformation}>>}
 */
function deleteCommand(sessionId, commandId) {
    return fetch(`${API_URL}/sessions/${sessionId}/commands/${commandId}`, {
        method: 'DELETE',
    })
    .then(response => response.json())
}

/**
 * Sets a session's accessibility.
 *
 * @param {string}  sessionId
 * @param {boolean} isPublic
 *
 * @return {Promise<ApiResponse<undefined>>}
 */
function setSessionAccessibility(sessionId, isPublic) {
    const body = urlEncodedBody({
        is_public: isPublic,
    });

    return fetch (`${API_URL}/sessions/${sessionId}/accessibility`, {
        method: 'PUT',
        body,
    })
    .then(response => response.json())
}

/**
 * Signature of the callback for the function checkSessionAvailability() below.
 *
 * @callback checkSessionCallback
 * @param {boolean} isValid
 * @param {string}  errorMessage
 */

/**
 * Checks if a session name is valid and available.
 *
 * @param {string}               sessionId
 * @param {checkSessionCallback} callback
 */
function checkSessionAvailability(sessionId, callback) {
    const isValid = sessionId.match(/^[a-zA-Z0-9-]+$/);

    if (!isValid) {
        callback(false, "Nom invalide");
        return;
    }

    pingSession(sessionId)
    .then(res => {
        const sessionFound = res.success;
        const message = sessionFound ? "Nom déjà existant" : "";
        callback(!sessionFound, message);
    })
}

module.exports = {
    createSession,
    renameSession,
    deleteSession,
    resetSession,
    runCode,
    runMultipleCode,
    editCommand,
    insertCommand,
    insertMultipleCommands,
    deleteCommand,
    setSessionAccessibility,
    checkSessionAvailability,
}
